mkdir /work
cd /work
apt-get update
apt-get install -y wget
wget -nv -O horizon.tar.gz https://github.com/stellar/go/releases/download/horizon-v1.0.1/horizon-v1.0.1-linux-amd64.tar.gz
tar -xvzf horizon.tar.gz
mv horizon-v1.0.1-linux-amd64 horizon
ln -s /work/horizon/horizon /usr/bin/horizon
