# Segun Stellar Docker

Docker Scripts/Images that starts seven docker containers and a load balancer.

The Images used can be found in https://drive.google.com/drive/folders/1E2fy-E1WyejPqhtn34rOwtnMDLTUE2yw?usp=sharing

You can load the images into docker using

<code>
    docker load < stellar_node.tar
    docker load < stellar_db.tar
</code>

After that then run

<code>
    ./start.sh first_run
    tail -f /tmp/sevunup.log
</code>

Wait for all the nodes to start up  properly then run

<code>
    ./start.sh restart
    tail -f /tmp/sevunup.log
</code>

You can connect to the Horizon Loadbalancer  on

http://localhost:8442

A directory called /Trash/StellarNodes/ is assumed to exist and writable by your  user otherwise you can edit the docker-compose scripts and change the directory