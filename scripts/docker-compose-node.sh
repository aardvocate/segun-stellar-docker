#!/bin/bash
#N=$1

for N in 2 3 4 5 6 7; do
    NODE="StellarNode1"
    REPLACE_NODE="StellarNode$N"
    DB="StellarDB1"
    REPLACE_DB="StellarDB$N"
    NODE_PORT="11721"
    REPLACE_NODE_PORT="1172$N"
    DB_PORT="5441"
    REPLACE_DB_PORT="544$N"
    HORIZON="Horizon1"
    REPLACE_HORIZON="Horizon$N"
    HORIZON_PORT="8111"
    REPLACE_HORIZON_PORT="811$N"
    cat ./scripts/docker-compose-node.tmpl | sed "s/$NODE/$REPLACE_NODE/g" | sed "s/$DB/$REPLACE_DB/g" | sed "s/$NODE_PORT/$REPLACE_NODE_PORT/g" | sed "s/$DB_PORT/$REPLACE_DB_PORT/g" | sed "s/$HORIZON/$REPLACE_HORIZON/g" | sed "s/$HORIZON_PORT/$REPLACE_HORIZON_PORT/g"
done
